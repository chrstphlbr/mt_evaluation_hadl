# Scrum hADL Model

This repository hosts a simplified Scrum model in [hADL](https://bitbucket.org/christophdorn/hadl). It is part of the [Evaluation](https://bitbucket.org/chrstphlbr/mt_evaluation) of Christoph Laaber's [master thesis](https://bitbucket.org/chrstphlbr/mt_thesis/).

The hADL model was defined and generated with a neat [hADL Model DSL](https://bitbucket.org/christophdorn/hadleditor). The generated valid hADL model is in `src-gen/agile-hADL.xml`.

Caveat: For execution of a hADL collaboration instance, the hADL model is not sufficient. Therefor Surrogate information needs to be specified. A complete hADL model is part of the [Evaluation](https://bitbucket.org/chrstphlbr/mt_evaluation) project in the folder `src/main/resources/agile-hadl.xml`.