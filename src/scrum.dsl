package agile

hADL Model name: scrum {
	Structure name: obj {
		Artifact name: Sprint
		Artifact name: Story
			Actions [userResponsible]
		Artifact name: Wiki
		Artifact name: Chat
			Actions [inviteUser]
		
		Relations {
			[containsStory : Sprint references Story]
			[prev : Wiki references Wiki]
			[next : Wiki references Wiki]
		}
	}
	
	Structure name: col {
		Component name: AgileUser
			Actions [storyResponsible]
		Component name: DevUser
			Actions [inviteChat]
			
		Relations {
			[devUser : AgileUser depends on DevUser]
			[agileUser : DevUser depends on AgileUser]
		}
	}
	
	Structure name: links {
		Link [responsible : scrum.col.AgileUser.storyResponsible => scrum.obj.Story.userResponsible]
		Link [invite : scrum.col.DevUser.inviteChat => scrum.obj.Chat.inviteUser]
	}
}
